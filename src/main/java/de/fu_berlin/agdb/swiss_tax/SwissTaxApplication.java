package de.fu_berlin.agdb.swiss_tax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main spring boot application responsible to start all components
 */
@SpringBootApplication
public class SwissTaxApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwissTaxApplication.class, args);
    }

}
