package de.fu_berlin.agdb.swiss_tax.controller;

import de.fu_berlin.agdb.swiss_tax.UnzipService;
import de.fu_berlin.agdb.swiss_tax.model.Canton;
import de.fu_berlin.agdb.swiss_tax.model.MaritalStatus;
import de.fu_berlin.agdb.swiss_tax.model.Tariff;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST controller to handle incoming HTTP requests to this spring server.
 */
@RestController
public class TaxController {
    private List<String> tariffData;

    Logger log = LoggerFactory.getLogger(TaxController.class);

    @Autowired
    UnzipService unzipService;

    /**
     * when application starts this post contruct method will be called to set the taxLines from the zip file
     */
    @PostConstruct
    private void init() {
        log.info("unzip taxes file...");
        try {
            unzipService.unzip("taxes.zip");
            setTariffData(Files.lines(unzipService.load("taxes")).collect(Collectors.toList()));
            log.info("loaded {} lines from tax file", tariffData.size());
        } catch (IOException e) {
            log.warn("could not unzip tax file", e);
        }
    }

    /**
     * Returns tax rate for a person with the given details.
     *
     * @param maritalStatus single, married,..
     * @param children      number of children in household
     * @param soleEarner    if only one of married or civil union couple is working
     * @param amount        ammount of money earned per month
     * @param canton        place person pays taxes in Switzerland
     * @return tax rate for a person.
     */
    @GetMapping("/taxrate")
    public String getTaxRate(@RequestParam("maritalStatus") Character maritalStatus,
            @RequestParam("children") int children, @RequestParam("needy_people") int needyPeople,
            @RequestParam(value = "sole_earner", required = false) boolean soleEarner,
            @RequestParam("amount") double amount, @RequestParam("canton") String canton) {
        // TODO: implement
        return String
                .format("Could not find tax rate for a %s in canton %s",
                        MaritalStatus.fromCode(maritalStatus).getDescription(),
                        Canton.fromCode(canton).getName());
    }

    /**
     * Return the tariff code for taxes.
     *
     * @param children      number of children in household
     * @param needyPeople   number of people that require support
     * @param maritalStatus single, married,..
     * @param soleEarner    if only one of married or civil union couple is working
     * @return the tariff code for taxes
     */
    protected Tariff getTariff(int children, int needyPeople, MaritalStatus maritalStatus, boolean soleEarner) {
        // TODO: implement
        return null;
    }


    /**
     * Returns tax rate for a person with the given details.
     *
     * @param tariff   to be applied, (A, B, C, H..)
     * @param children number of children in household
     * @param amount   ammount of money earned per month
     * @param canton   place person pays taxes in Switzerland
     * @return numeric value of the tax rate for a person.
     */
    protected Double calculateTaxRate(Tariff tariff, int children, double amount, Canton canton) {
        // TODO: implement
        return 0.0;
    }

    public void setTariffData(List<String> tariffData) {
        this.tariffData = tariffData;
    }
}
